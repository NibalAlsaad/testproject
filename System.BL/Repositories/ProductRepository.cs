﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.BL.Context;
using System.BL.Entities;
using System.BL.Repositories.Interfaces;
using System.BL.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.BL.Repositories
{
   public class ProductRepository : RepositoryBase<Products, ProductViewModel, Guid>, IProductRepository
    {
        TechContext _context;
        IMapper _mapper;
        public ProductRepository(TechContext context, IMapper mapper) : base(context, mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async override Task<IEnumerable<ProductViewModel>> GetAllAsync()
        {
            var res = await _context.Products.Include(prop => prop.ProductImages).ToListAsync();
           return _mapper.Map<IEnumerable<ProductViewModel>>(res);
        }
        public async override Task<ProductViewModel> GetAsync(Guid id)
        {
            var product = await _context.Products.FindAsync(id);
            await _context.Entry(product).Collection(p => p.ProductImages)
                 .LoadAsync();

            return _mapper.Map< ProductViewModel>(product);
        }
        public List<ProductImages> AddProductImages(Guid id,List<string> paths)
        {
            var productImages = new List<ProductImages>();
            for (int i=0; i < paths.Count; i++)
            {
                var item = new ProductImageViewModel { ProductId = id, Path = paths[i] };
                var tableItem=_mapper.Map<ProductImages>(item);
                productImages.Add(tableItem);
               
            }
            return productImages;
        }
        public async override Task AddAsync(ProductViewModel item)
        {
            //   return base.AddAsync(item);
            var tableItem = _mapper.Map<Products>(item);
            var r=await _context.Products.AddAsync(tableItem);
            await _context.SaveChangesAsync();
            var listImages = new List<string>(item.SetImages);
            var tableListImages = AddProductImages(r.Entity.Id, listImages);
            await _context.ProductImages.AddRangeAsync(tableListImages);
            await _context.SaveChangesAsync();
        }
        public async Task deleteProductImages( List<ProductImageViewModel> items)
        {
            var productimagesTable = _mapper.Map<List<ProductImages>>(items);
           
                _context.ProductImages.RemoveRange(productimagesTable);
               await _context.SaveChangesAsync();
        
        }
        public async override Task RemoveItem(ProductViewModel item)
        {
            await deleteProductImages(item.ProductImages.ToList());         
            var entity = _mapper.Map<Products>(item);
            entity.ProductImages = new List<ProductImages>();
            _context.Products.Remove(entity);
            await _context.SaveChangesAsync();

        }
    }
}