﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.BL.Context;
using System.BL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace System.BL.Repositories
{
    public class RepositoryBase<TEntity, TModel, TKey>
        : IRepository<TEntity, TModel, TKey> where TEntity : class
    {
        TechContext _context;
        IMapper _mapper;
        private DbSet<TEntity> dataTable
        {
            get
            {
                return _context.Set<TEntity>();
            }
        }
        public RepositoryBase(TechContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public virtual async Task<IEnumerable<TModel>> GetAllAsync()
        {
            var res = await dataTable.ToListAsync();
            return _mapper.Map<IEnumerable<TModel>>(res);
        }

        public virtual async Task<TModel> GetAsync(TKey id)
        {
            var res = await dataTable.FindAsync(id);
            return _mapper.Map<TModel>(res);
        }

        public async Task<IEnumerable<TModel>> FindAsync(Expression<Func<TEntity, bool>> predicate)
        {
            var res = await dataTable.Where(predicate).ToListAsync();
            return _mapper.Map<IEnumerable<TModel>>(res);
        }

        public async Task<TModel> SingleOrDefaultAsync(Expression<Func<TEntity, bool>> predicate)
        {
            var res = await dataTable.SingleOrDefaultAsync(predicate);
            return _mapper.Map<TModel>(res);
        }

        public virtual async Task AddAsync(TModel item)
        {
            var entity = _mapper.Map<TEntity>(item);
            await dataTable.AddAsync(entity);
        }
        public virtual async Task<TModel> AddModelAsync(TModel item)
        {
            var entity = _mapper.Map<TEntity>(item);
            await dataTable.AddAsync(entity);
            await _context.SaveChangesAsync();
            return _mapper.Map<TModel>(entity);

        }
        public virtual async Task AddRangeAsync(IEnumerable<TModel> items)
        {
            var entities = _mapper.Map<IEnumerable<TEntity>>(items);
            await dataTable.AddRangeAsync(entities);
        }

        public virtual Task UpdateItem(TModel item)
        {
            var entity = _mapper.Map<TEntity>(item);
            return Task.Run(() => dataTable.Update(entity));

        }

        public virtual Task RemoveItem(TModel item)
        {
            var entity = _mapper.Map<TEntity>(item);
            return Task.Run(() => dataTable.Remove(entity));
        }
        public virtual Task RemoveItemById(TKey itemId)
        {
            var entity = _mapper.Map<TEntity>(dataTable.Find(itemId));
            return Task.Run(() => dataTable.Remove(entity));
        }

        public virtual Task RemoveRangeItems(IEnumerable<TModel> items)
        {
            var entities = _mapper.Map<IEnumerable<TEntity>>(items);
            return Task.Run(() => dataTable.RemoveRange(entities));
        }

    }
}
