﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace System.BL.Repositories.Interfaces
{
    public interface IRepository<TEntity, TModel, TKey>
    {
        Task<IEnumerable<TModel>> GetAllAsync();
        Task<TModel> GetAsync(TKey id);
        Task<IEnumerable<TModel>> FindAsync(Expression<Func<TEntity, bool>> predicate);
        Task<TModel> SingleOrDefaultAsync(Expression<Func<TEntity, bool>> predicate);
        Task<TModel> AddModelAsync(TModel item);
        Task AddAsync(TModel item);
        Task AddRangeAsync(IEnumerable<TModel> items);
        Task UpdateItem(TModel item);
        Task RemoveItem(TModel item);
        Task RemoveItemById(TKey itemId);
        Task RemoveRangeItems(IEnumerable<TModel> items);
    }
}
