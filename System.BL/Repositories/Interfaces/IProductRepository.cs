﻿using System;
using System.BL.Entities;
using System.BL.ViewModels;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace System.BL.Repositories.Interfaces
{
   public interface IProductRepository : IRepository<Products, ProductViewModel, Guid>
    {
        Task deleteProductImages(List<ProductImageViewModel> items);
        List<ProductImages> AddProductImages(Guid id, List<string> paths);
    }
}
