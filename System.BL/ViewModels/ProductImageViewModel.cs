﻿using System;
using System.Collections.Generic;
using System.Text;

namespace System.BL.ViewModels
{
    public class ProductImageViewModel
    {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public string ProductTitle { get; set; }
        public string Path { get; set; }
    }
}
