﻿using Microsoft.AspNetCore.Http;
using System;
using System.BL.Entities;
using System.Collections.Generic;
using System.Text;

namespace System.BL.ViewModels
{
    public class FileUpload
    {
        public IFormFile files { get; set; }
    }
    public class ProductViewModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public IEnumerable<ProductImageViewModel> ProductImages { get; set; }
        public IEnumerable<string> SetImages { get; set; }
    }
}
