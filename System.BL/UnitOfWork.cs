﻿
using AutoMapper;
using System.BL.Context;
using System.BL.Repositories;
using System.BL.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace System.BL.Context
{
    public class UnitOfWork : IUnitOfWork
    {

        TechContext _context;
        IMapper _mapper;
        public UnitOfWork(TechContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
            productRepository = new ProductRepository(_context, _mapper);

        }

        public IProductRepository productRepository { get; }

        public int commit()
        {
            return _context.SaveChanges();
        }

        public async Task<int> CommitAsync()
        {
            return await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
