﻿using System;
using System.Collections.Generic;

namespace System.BL.Entities
{
    public partial class Products
    {
        public Products()
        {
            ProductImages = new HashSet<ProductImages>();
        }

        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }

        public Products IdNavigation { get; set; }
        public Products InverseIdNavigation { get; set; }
        public ICollection<ProductImages> ProductImages { get; set; }
    }
}
