﻿using System;
using System.Collections.Generic;

namespace System.BL.Entities
{
    public partial class ProductImages
    {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public string Path { get; set; }

        public Products Product { get; set; }
    }
}
