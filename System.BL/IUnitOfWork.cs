﻿//using System.BL.Repositories.Interfaces;
using System;
using System.BL.Repositories.Interfaces;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace System.BL
{
   public interface IUnitOfWork
    {
       IProductRepository productRepository { get; }
        int commit();
        Task<int> CommitAsync();
        void Dispose();
    }
}
