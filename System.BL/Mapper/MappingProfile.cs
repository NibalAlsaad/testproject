﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.BL.Entities;
using System.BL.ViewModels;

namespace System.BL.Mappers
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<ProductViewModel, Products>();
            CreateMap<Products, ProductViewModel > ();
            CreateMap<ProductImages, ProductImageViewModel > ()
            .ForMember(dest => dest.ProductTitle, opt => opt.MapFrom(src => src.Product.Title));
            CreateMap<ProductImageViewModel, ProductImages>();

            //        CreateMap<Employees, EmployeeViewModel>()
            //.ForMember(dest => dest.DepartmentName, opt => opt.MapFrom(src => src.Department.Name))
            //.ForMember(dest => dest.PositionName, opt => opt.MapFrom(src => src.Position.Name));
            //        CreateMap<EmployeeViewModel, Employees>();

        }
    }
}