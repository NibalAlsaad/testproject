﻿using System;
using System.API.Healper;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using System.BL.ViewModels;

namespace System.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : Controller
    {
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;
        TokenProvider _tokenProvider;

        public AccountController(SignInManager<IdentityUser> signInManager,
            UserManager<IdentityUser> userManager,
            TokenProvider tokenProvider)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _tokenProvider = tokenProvider;
        }



        [HttpPost("/api/login")]
        public async Task<IActionResult> Login([FromBody] LoginViewModel userInfo)
        {
            if (userInfo == null)
            {
                return BadRequest("Invalid client request");
            }
            var user = await _userManager.FindByNameAsync(userInfo.userName);
            if (user != null)
            {
                var result = await _signInManager.PasswordSignInAsync(user, userInfo.password, false, false);

                if (result.Succeeded)
                {
                    var tokenString = _tokenProvider.createToken();
                    return Ok(new
                    {
                        token = tokenString
                    });
                }
                else
                {
                    return Unauthorized();
                }
            }
            return BadRequest("this client not exist");
        }
        [HttpPost("/api/register")]
        public async Task<IActionResult> Register([FromBody] LoginViewModel userInfo)
        {
            var user = new IdentityUser { UserName = userInfo.userName };
            var result = await _userManager.CreateAsync(user, userInfo.password);
            if (result.Succeeded)
            {
                var tokenString = _tokenProvider.createToken();
                return Ok(new
                {
                    token = tokenString
                });
            }

            return BadRequest(new
            {

                message = "password length must be 6 atleast and password must contain 1 digit atleast "

            });
        }
    }
}