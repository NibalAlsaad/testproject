﻿using System;
using System.BL;
using System.BL.ViewModels;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace System.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
 
    public class ProductController : Controller
    {
        IUnitOfWork _unitOfWork; 
        public ProductController(IUnitOfWork unitOfWork, IHostingEnvironment environment)
        {
            _unitOfWork = unitOfWork;
        }
        [HttpGet("/api/product/products")]
        public async Task<IActionResult> GetAllproducts()
        {
            var data = await _unitOfWork.productRepository.GetAllAsync();
            return Ok(data);
        }
        [HttpGet("/api/product/{id}")]
        public async Task<IActionResult> Getproduct(Guid id)
        {
            var data = await _unitOfWork.productRepository.GetAsync(id);
            return Ok(data);
        }

        [HttpPost("/api/product/Add")]
        public async Task<IActionResult> Addproduct([FromBody] ProductViewModel productModel)
        {
            try
            {
                if (productModel == null)
                    return BadRequest("parameter not exist");
                await _unitOfWork.productRepository.AddAsync(productModel);
       //         await _unitOfWork.CommitAsync();
               var res = await _unitOfWork.productRepository.GetAllAsync();
                return Ok(new { code = 0, data = res });
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpPut("/api/product/Edit")]
        public async Task<IActionResult> Editproduct([FromBody] ProductViewModel productModel)
        {
            if (productModel == null)
                return BadRequest("parameter not exist");
            await _unitOfWork.productRepository.UpdateItem(productModel);
            await _unitOfWork.CommitAsync();
            return Ok();

        }

        [HttpPost("/api/product/Delete")]
        public async Task<IActionResult> Deleteproduct([FromBody] ProductViewModel product)
        {
            if (product == null)
                return BadRequest("parameter not exist");
            await _unitOfWork.productRepository.RemoveItem(product);
       //    await _unitOfWork.CommitAsync();
            return Ok();

        }      

    }
}